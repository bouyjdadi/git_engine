import React, { Component } from 'react'
import Users from './Components/Users/Users'
import Navbar from './partials/Navbar'
import './App.css';


export default class App extends Component {
  render() {
    return (
      <div>
        <Navbar/>
        <Users />
      </div>
    )
  }
}
