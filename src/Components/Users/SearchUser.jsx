import React, { Component } from 'react'

class SearchUser extends Component {
    state={
        search:""
    }
    handelForm=(e)=>{
        this.setState({
            search:e.target.value
        })
    }
    SearchUsers=(e)=>{
       e.preventDefault()
       this.props.getUserSearch(this.state.search)
    }
  render() {
    return (
        <form onSubmit={this.SearchUsers}>
            <div className="form-group ">
                <input value={this.state.search} type="text" onChange={this.handelForm} placeholder='Search users...' className='form-control'/>
            </div>
            <button className='btn btn-danger m-4 float-end'>Search</button>
        </form>
    )
  }
}
export default  SearchUser;