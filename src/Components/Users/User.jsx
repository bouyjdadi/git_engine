import React, { Component } from 'react'

export default class User extends Component {
  render() {
    return (
      <div>
        <div className="card mb-4" >
                <img src={this.props.user.avatar_url} className="card-img-top" alt="..." />
                <div className="card-body">
                  <h5 className="card-title">{this.props.user.login}</h5>
                  <p className="card-text">
                    <a href={this.props.user.html_url} className="btn btn-success">
                      show more
                    </a>
                    <a href={this.props.user.repos_url} className="btn btn-warning ms-3 my-2">
                      Go repo
                    </a>
                  </p>
                </div>
              </div>
      </div>
    )
  }
}
