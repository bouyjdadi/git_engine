import React, { Component } from "react";
import axios from "axios";
import SearchUser from "./SearchUser";
import User from "./User";
class Users extends Component {
  constructor() {
    super();
    this.state = {
      users: [
        {
          login: "mojombo",
          id: 1,
          avatar_url: "https://avatars.githubusercontent.com/u/1?v=4",
          html_url: "https://github.com/mojombo",
          repos_url: "https://api.github.com/users/mojombo/repos",
        },
        {
          login: "defunkt",
          id: 2,
          avatar_url: "https://avatars.githubusercontent.com/u/2?v=4",
          html_url: "https://github.com/defunkt",
          repos_url: "https://api.github.com/users/defunkt/repos",
        },
      ],
    };
  }
  getUsers=()=>{
    axios.get('https://api.github.com/users')
    .then(respense=>{
      this.setState({users:respense.data})
    })
  }
  componentDidMount(){
    this.getUsers()
  }
  searchUseFromGit=(data)=>{
      if(data!=""){
        axios.get(`https://api.github.com/search/users?q=${data}`)
          .then(response=>{
          this.setState({
            users:response.data.items
          })
          })
      }
  }
  render() {
    return (
      <div className="container mt-5 mb-5 ">
        <div className="row">
          <div className="col-md-12">
            <SearchUser getUserSearch={this.searchUseFromGit} />
          </div>
        </div>
        <div className="row">
          {this.state.users.map((user) => {
            return ( 
            <div className="col-md-3" key={user.id}>
              <User user={user} />
            </div>)
          })}
        </div>
      </div>
    );
  }
}
export default  Users;