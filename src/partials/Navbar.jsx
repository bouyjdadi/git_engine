import React, { Component } from 'react'

export default class Navbar extends Component {
  render() {
    return (
      <div>
            <nav className="navbar navbar-expand-sm navbar-dark bg-dark ">
                <div className="container-fluid">
                    <a className="navbar-brand" href="../index.js">Git engine</a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarID"
                        aria-controls="navbarID" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarID">
                        <div className="navbar-nav">
                            <a className="nav-link active" aria-current="page" href="../index.js">Users</a>
                            
                        </div>
                    </div>
                </div>
            </nav>
      </div>
    )
  }
}
